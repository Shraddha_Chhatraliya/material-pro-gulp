$(function(){
    $('#themecolors').on('click', 'a', function () {
        $('#themecolors li a').removeClass('working');
        $(this).addClass('working')
    });

    // Default ThemeColor
    function handledefault() {
        $('#light-default').on('click', function () {
            $('body').attr("data-themecolor", 'light-default');
        });
    };
    handledefault();

    // Default ThemeColor
    function handlegreen() {
        $('#light-green').on('click', function () {
            $('body').attr("data-themecolor", 'light-green');
        });
    };
    handlegreen();

    // Default ThemeColor
    function handlered() {
        $('#light-red').on('click', function () {
            $('body').attr("data-themecolor", 'light-red');
        });
    };
    handlered();

    // Default ThemeColor
    function handleblue() {
        $('#light-blue').on('click', function () {
            $('body').attr("data-themecolor", 'light-blue');
        });
    };
    handleblue();

    // Default ThemeColor
    function handlepurple() {
        $('#light-purple').on('click', function () {
            $('body').attr("data-themecolor", 'light-purple');
        });
    };
    handlepurple();

    // Default ThemeColor
    function handlemegna() {
        $('#light-megna').on('click', function () {
            $('body').attr("data-themecolor", 'light-megna');
        });
    };
    handlemegna();



    // Default ThemeColor
    function handledarkdefault() {
        $('#dark-default').on('click', function () {
            $('body').attr("data-themecolor", 'dark-default');
        });
    };
    handledarkdefault();

    // Default ThemeColor
    function handledarkgreen() {
        $('#dark-green').on('click', function () {
            $('body').attr("data-themecolor", 'dark-green');
        });
    };
    handledarkgreen();

    // Default ThemeColor
    function handledarkred() {
        $('#dark-red').on('click', function () {
            $('body').attr("data-themecolor", 'dark-red');
        });
    };
    handledarkred();

    // Default ThemeColor
    function handledarkblue() {
        $('#dark-blue').on('click', function () {
            $('body').attr("data-themecolor", 'dark-blue');
        });
    };
    handledarkblue();

    // Default ThemeColor
    function handledarkpurple() {
        $('#dark-purple').on('click', function () {
            $('body').attr("data-themecolor", 'dark-purple');
        });
    };
    handledarkpurple();

    // Default ThemeColor
    function handledarkmegna() {
        $('#dark-megna').on('click', function () {
            $('body').attr("data-themecolor", 'dark-megna');
        });
    };
    handledarkmegna();

    $(window).on("load", function () {
        var themecolor = $('body').attr('data-themecolor');
        if (typeof themecolor !== typeof undefined && themecolor !== false) {
            switch (themecolor) {
                case 'light-default':
                    $('#light-default').prop("checked", !0);
                    break;
                case 'light-green':
                    $('#light-green').prop("checked", !0);
                    break;
                case 'light-red':
                    $('#light-red').prop("checked", !0);
                    break;
                case 'light-blue':
                    $('#light-blue').prop("checked", !0);
                    break;
                case 'light-purple':
                    $('#light-purple').prop("checked", !0);
                    break;
                case 'light-megna':
                    $('#light-megna').prop("checked", !0);
                    break;
                case 'dark-default':
                    $('#dark-default').prop("checked", !0);
                    break;
                case 'dark-green':
                    $('#dark-green').prop("checked", !0);
                    break;
                case 'dark-red':
                    $('#dark-red').prop("checked", !0);
                    break;
                case 'dark-blue':
                    $('#dark-blue').prop("checked", !0);
                    break;
                case 'dark-purple':
                    $('#dark-purple').prop("checked", !0);
                    break;
                case 'dark-megna':
                    $('#dark-megna').prop("checked", !0);
                    break;
                default:
            }
        }
    });
});